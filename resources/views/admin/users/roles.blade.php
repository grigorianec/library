@extends('layouts.app')

@section('content')
    <form method="POST" action="{{ route('admin.user.roles-edit', $user) }}">
        @csrf
        @foreach($roles as $role)
            <div class="form-group d-flex justify-content-between">
                <label for="role" class="col-form-label">{{ $role }}</label>
                <input type="checkbox" id="role" class="form-control align-self-auto w-75" name="role[]" value="{{ $role }}"
                @if(in_array($role, $user->role))
                        checked="checked"
                @endif>
            </div>
        @endforeach

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection