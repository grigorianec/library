@extends('layouts.app')

@section('content')
    @include('admin.users._nav')

    <form method="POST" action="{{ route('admin.authors.update', $author) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name" class="col-form-label">Name</label>
            <input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $author->name) }}" required>
            @if ($errors->has('name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>

        <div class="form-group">
            <label for="surname" class="col-form-label">Surname</label>
            <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname', $author->surname) }}" required>
            @if ($errors->has('surname'))
                <span class="invalid-feedback"><strong>{{ $errors->first('surname') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="biography">Biography</label>
            <textarea class="form-control" {{ $errors->has('biography') ? ' is-invalid' : '' }} id="biography" rows="3" name="biography">
                {{ old('biography', $author->biography) }}
            </textarea>
            @if ($errors->has('biography'))
                <span class="invalid-feedback"><strong>{{ $errors->first('biography') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
    <form method="POST" action="{{ route('admin.authors.delete', $author) }}">
        @csrf
        @method('DELETE')
        <div class="form-group">
            <button type="submit" class="btn btn-danger">Delete</button>
        </div>
    </form>
@endsection