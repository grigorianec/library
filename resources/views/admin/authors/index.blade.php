@extends('layouts.app')

@section('content')
    @include('admin.authors._nav')
    <div>
        <a href="{{ route('admin.authors.create') }}" class="btn btn-success">
            Add Author
        </a>
    </div>
    <div class="card mb-3">
        <div class="card-header">Filter</div>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="id" class="col-form-label">ID</label>
                            <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="name" class="col-form-label">Name</label>
                            <input id="name" class="form-control" name="name" value="{{ request('name') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="email" class="col-form-label">Surname</label>
                            <input id="email" class="form-control" name="email" value="{{ request('email') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br />
                            <button type="submit" class="btn btn-primary">Search</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Status</th>
            <th>Role</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($authors as $author)
            <tr>
                <td>{{ $author->id }}</td>
                <td><a href="{{ route('admin.authors.show', $author) }}">{{ $author->name }}</a></td>
                <td><a href="{{ route('admin.authors.show', $author) }}">{{ $author->surname }}</a></td>
                <td>
                   {{ $author->biography }}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>


    {{ $authors->links() }}
@endsection