@extends('layouts.app')

@section('content')
    @include('admin.users._nav')

    <div class="d-flex flex-row mb-3">
        <a href="{{ route('admin.books.edit', $book) }}" class="btn btn-primary mr-1">Edit</a>
    </div>

    <table class="table table-bordered table-striped">
        <tbody>
        <tr>
            <th>ID</th><td>{{ $book->id }}</td>
        </tr>
        <tr>
            <th>Name</th><td>{{ $book->name }}</td>
        </tr>
        <tr>
            <th>Description</th><td>{{ $book->description }}</td>
        </tr>
        <tr>
            <th>Link</th><td><a href="{{ $book->link }}">Перейти</a></td>
        </tr>
        <tbody>
        </tbody>
    </table>

@endsection