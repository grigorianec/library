@extends('layouts.app')

@section('content')
    @include('admin.books._nav')
    <div>
        <a href="{{ route('admin.books.create') }}" class="btn btn-success">
            Add Book
        </a>
    </div>

    @include('admin.books.searchAjax')
@endsection
