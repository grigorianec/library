@extends('layouts.app')

@section('content')
    @include('admin.users._nav')

    <form method="POST" action="{{ route('admin.books.store') }}" enctype="multipart/form-data">
        @csrf

        <div class="form-group">
            <label for="name" class="col-form-label">Name</label>
            <input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>
            @if ($errors->has('name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" {{ $errors->has('description') ? ' is-invalid' : '' }} id="description" rows="3" name="description">
                {{ old('description') }}
            </textarea>
            @if ($errors->has('description'))
                <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="link" class="col-form-label">Add Book</label>
            <input id="link" type="file" class="form-control{{ $errors->has('link') ? ' is-invalid' : '' }}" name="link" value="{{ old('link') }}">
            @if ($errors->has('link'))
                <span class="invalid-feedback"><strong>{{ $errors->first('link') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="author" class="col-form-label">Authors</label>
            <select id="author" class="form-control{{ $errors->has('authors') ? ' is-invalid' : '' }}" name="author">
                @foreach ($authors as $value => $label)
                    <option value="{{ $label->id }}">{{ $label->name . ' ' . $label->surname }}</option>
                @endforeach;
            </select>
            @if ($errors->has('authors'))
                <span class="invalid-feedback"><strong>{{ $errors->first('authors') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="categories" class="col-form-label">Categories</label>
            <select id="categories" class="form-control{{ $errors->has('categories') ? ' is-invalid' : '' }}" name="categories[]"  multiple>
                @foreach ($categories as $key => $label)
                    <option value="{{ $catArray[] = $label->id }}">{{ $label->name }}</option>
                @endforeach;
            </select>
            @if ($errors->has('$categories'))
                <span class="invalid-feedback"><strong>{{ $errors->first('$categories') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endsection