<div id="table_data">
    <div class="card mb-3">
        <div class="card-header">Filter</div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-1">
                    <div class="form-group">
                        <label for="categories" class="col-form-label">Category</label>
                        <input id="categories" class="form-control" name="categories">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="author" class="col-form-label">Author Surname</label>
                        <input id="author" class="form-control" name="author" >
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="col-form-label">&nbsp;</label><br />
                        <button id="search" class="btn btn-primary">Search</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Link</th>
        <th>Categories</th>
        <th>Author</th>
    </tr>
    </thead>
    <tbody>

    @foreach ($books as $book)
        <tr>
            <td>{{ $book->id }}</td>
            <td><a href="{{ route('admin.books.show', $book) }}">{{ $book->name }}</a></td>
            <td><a href="{{ route('admin.books.show', $book) }}">{{ $book->description }}</a></td>
            <td>
                @if($book->link !== ' ')
                    <a href="{{ $book->link }}">Перейти</a>
                @endif
            </td>
            <td>
                @foreach ($book->categories as $category)
                    @if ($loop->iteration > 1)
                        |
                    @endif
                    {{ $category->name }}
                @endforeach
            </td>
            <td>
                {{ $book->author->name . ' ' . $book->author->surname }}
            </td>
        </tr>
    @endforeach

    </tbody>
</table>
</div>