@extends('layouts.app')

@section('content')
{{--    @include('admin.users._nav')--}}

    <form method="POST" action="{{ route('admin.books.update', $book) }}">
        @csrf
        @method('PUT')

        <div class="form-group">
            <label for="name" class="col-form-label">Name</label>
            <input id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $book->name) }}" required>
            @if ($errors->has('name'))
                <span class="invalid-feedback"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" {{ $errors->has('description') ? ' is-invalid' : '' }} id="description" rows="3" name="description">
                {{ old('description', $book->description) }}
            </textarea>
            @if ($errors->has('description'))
                <span class="invalid-feedback"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
        <div class="d-flex">
            <div class="form-group mr-2">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>

        </div>
    </form>
    <form method="POST" action="{{ route('admin.books.delete', $book) }}">
        @csrf
        @method('DELETE')
        <div class="form-group">
            <button type="submit" class="btn btn-danger">Delete</button>
        </div>
    </form>
@endsection