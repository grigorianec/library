@extends('layouts.app')

@section('content')
    @include('admin.books._nav')
    <div>
        <a href="{{ route('admin.categories.create') }}" class="btn btn-success">
            Add Category
        </a>
    </div>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($categories as $category)
            <tr>
                <td>{{ $category->id }}</td>
                <td><a href="{{ route('admin.categories.show', $category) }}">{{ $category->name }}</a></td>
            </tr>
        @endforeach

        </tbody>
    </table>


    {{ $categories->links() }}
@endsection