/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

$(document).ready(function(){

    function clear_icon()
    {
        $('#id_icon').html('');
        $('#full_name_icon').html('');
    }

    function fetch_data(category, author)
    {
        $.ajax({
            url:"/admin/book/ajax?category="+category+"&author="+author,
            method: "GET",
            // data: new FormData(this),
            // contentType: false,
            // processData: false,
            // dataType: "json",
            success:function(data)
            {
                // $('tbody').html('');
                $('#table_data').html(data)
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(xhr.responseText);
                alert(thrownError);
            }
        })
    }

    $(document).on('click', '#search', function(){
        var category = $('#categories').val();
        var author = $('#author').val();
        fetch_data(category, author);
    });
});
