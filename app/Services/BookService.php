<?php

namespace App\Services;

use App\Entity\Book;
use App\Entity\BookCategory;
use Illuminate\Http\Request;

class BookService
{

	public function getIds($array):array
	{
		$ids = [];
		foreach ( $array as $item ) {
			 $ids[] = $item->id;
		}
		return $ids;
	}

	public function getBookIds($array):array
	{
		$ids = [];
		foreach ( $array as $item ) {
			$ids[] = $item->book_id;
		}
		return $ids;
	}

	public function createBookCategory($categories, Book $book):void
	{
		foreach ($categories as $category){
			BookCategory::create([
				'category_id' => $category,
				'book_id' => $book->id
			]);
		}
	}

	public function fileHandle(Request $request, Book $book):void
	{
		$path = null;
		if ($request['link'] !== null){
			$image = $request->file('link');
			$new_name = rand() . '.' . $image->getClientOriginalExtension();
			$image->move(public_path('build/books'), $new_name);
			$path = '/build/books/' . $new_name;
			$book->update(['link' => $path]);
		}
	}

}