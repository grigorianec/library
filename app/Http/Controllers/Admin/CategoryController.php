<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Category;
use App\Http\Requests\Category\CreateCategoryRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
	    $query = Category::orderByDesc('id');

	    $categories = $query->paginate(20);

	    return view('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
	    return view('admin.categories.create');
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateCategoryRequest $request
	 *
	 * @return Response
	 */
    public function store(CreateCategoryRequest $request)
    {
	    $category = Category::create(
		    [
			    'name'  => $request['name'],
		    ]
	    );

	    return redirect()->route('admin.categories.show', $category);
    }

	/**
	 * Display the specified resource.
	 *
	 * @param Category $category
	 *
	 * @return Response
	 */
    public function show(Category $category)
    {
	    return view('admin.categories.show', compact('category'));
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Category $category
	 *
	 * @return void
	 */
    public function edit(Category $category)
    {
	    return view('admin.categories.edit', compact('category'));
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param CreateCategoryRequest $request
	 * @param Category $category
	 *
	 * @return Response
	 */
    public function update(CreateCategoryRequest $request, Category $category)
    {
	    $category->update(
		    [
			    'name'  => $request['name'],
		    ]
	    );
	    return redirect()->route('admin.categories.show', $category);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Category $category
	 *
	 * @return Response
	 * @throws \Exception
	 */
    public function destroy(Category $category)
    {
	    $category->delete();

	    return redirect()->route('admin.categories.index');
    }
}
