<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Author;
use App\Http\Requests\Author\CreateRequest;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class AuthorController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
    public function index(Request $request)
    {
	    $query = Author::orderByDesc('id');
	    $query = Author::has('reviews','>','3');

	    if (!empty($value = $request->get('id'))) {
		    $query->where('id', $value);
	    }

	    if (!empty($value = $request->get('name'))) {
		    $query->where('name', 'like', '%' . $value . '%');
	    }

	    if (!empty($value = $request->get('surname'))) {
		    $query->where('role', 'like', '%' . $value . '%');
	    }

	    $authors = $query->paginate(20);


	    return view('admin.authors.index', compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
	    return view('admin.authors.create');
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateRequest $request
	 *
	 * @return Response
	 */
    public function store(CreateRequest $request)
    {
	    $author = Author::create(
	    	[
			    'name'  => $request['name'],
			    'surname' => $request['surname'],
			    'biography' => $request['biography'],
		    ]
	    );

	    return redirect()->route('admin.authors.show', $author);
    }

	/**
	 * Display the specified resource.
	 *
	 * @param Author $author
	 *
	 * @return Response
	 */
    public function show(Author $author)
    {
	    return view('admin.authors.show', compact('author'));
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Author $author
	 *
	 * @return Response
	 */
    public function edit(Author $author)
    {
	    return view('admin.authors.edit', compact('author'));
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param CreateRequest $request
	 * @param Author $author
	 *
	 * @return void
	 */
    public function update(CreateRequest $request, Author $author)
    {
	    $author->update([
		    'name' => $request['name'],
		    'surname' => $request['surname'],
		    'biography' => $request['biography'],
	    ]);

	    return redirect()->route('admin.authors.show', $author);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Author $author
	 *
	 * @return void
	 * @throws Exception
	 */
    public function destroy(Author $author)
    {
	    $author->delete();

	    return redirect()->route('admin.authors.home');
    }
}
