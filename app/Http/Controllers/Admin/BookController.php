<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use App\Entity\BookCategory;
use App\Http\Requests\Book\CreateBookRequest;
use App\Services\BookService;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class BookController extends Controller
{

	/**
	 * @var BookService
	 */
	private $service;

	public function __construct(BookService $service)
	{

		$this->service = $service;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 * @throws \Throwable
	 */
    public function index(Request $request)
    {
	    $query = Book::with( ['categories', 'author'] )->get();

	    if($request->ajax()) {
		    if ( ! empty( $value = $request->get( 'author' ) ) ) {
			    $authors = Author::authorLike( 'surname', $value );
			    $query = Book::with( [ 'author' ] )
			                 ->whereIn( 'author_id', $this->service->getIds($authors))->get();
		    }
		    if ( ! empty( $value = $request->get( 'category' ) ) ) {
			    $categories = Category::categoryLike('name', $value);
			    $bookCategories = BookCategory::whereIn( 'category_id', $this->service->getIds($categories) )->get();
			    $query = Book::whereIn( 'id', $this->service->getBookIds($bookCategories))->get();
		    }
		    $books = $query;
		    return view( 'admin.books.searchAjax', compact( 'books' ))->render();
	    }else{
		    $books = $query;
		    return view( 'admin.books.index', compact( 'books' ) );
	    }
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return void
	 */
    public function create()
    {
	    $categories = Category::all();
	    $authors = Author::all();
	    $catArray = [];
	    return view('admin.books.create', compact('categories', 'authors', 'catArray'));
    }

	/**
	 * Store a newly created resource in storage.
	 * @param CreateBookRequest $request
	 * @return Response
	 */
    public function store(CreateBookRequest $request)
    {
	    $book = Book::create(
		    [
			    'name'  => $request['name'],
			    'description' => $request['description'],
			    'link' => ' ',
			    'author_id' => $request['author'],
		    ]
	    );
	    $this->service->createBookCategory($request['categories'], $book);
	    $this->service->fileHandle($request, $book);

	    return redirect()->route('admin.books.show', $book);
    }

	/**
	 * Display the specified resource.
	 *
	 * @param Book $book
	 *
	 * @return Response
	 */
    public function show(Book $book)
    {
	    return view('admin.books.show', compact('book'));
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param Book $book
	 *
	 * @return Response
	 */
    public function edit(Book $book)
    {
	    return view('admin.books.edit', compact('book'));
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param CreateBookRequest $request
	 * @param Book $book
	 *
	 * @return Response
	 */
    public function update(CreateBookRequest $request, Book $book)
    {
	    $book->update(
		    [
			    'name'  => $request['name'],
			    'description' => $request['description'],
		    ]
	    );
	    $path = null;
	    $image = $request->file('link');
	    if ($request['link'] !== null){
		    $new_name = rand() . '.' . $image->getClientOriginalExtension();
		    $image->move(public_path('build/books'), $new_name);
		    $path = '/build/books/' . $new_name;
		    $book->update(['link' => $path]);
	    }
	    return redirect()->route('admin.books.show', $book);
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param Book $book
	 *
	 * @return Response
	 * @throws Exception
	 */
    public function destroy(Book $book)
    {
	    $book->delete();

	    return redirect()->route('admin.books.index');
    }
}
