<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $link
 * @property string $author_id
 **/
class Book extends Model
{
	protected $fillable = ['name', 'description', 'link', 'author_id'];

	protected $table = 'book';


	public function categories()
	{
		return $this->belongsToMany(Category::class);
	}

	public function author()
	{
		return $this->belongsTo(Author::class, 'author_id', 'id');
	}

}