<?php

namespace App\Entity;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $name
 * @method Builder categoryLike($column, $value)
 **/
class Category extends Model
{
	protected $fillable = ['name'];

	public $timestamps = false;
	protected $table = 'category';

	public function books()
	{
		return $this->belongsToMany(Book::class);
	}

	public function scopeCategoriesLike(Builder $query, $column, $value)
	{
		return $query->where( $column, 'like', "%$value%" )->get();
	}
}