<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $biography
 * @method Builder authorLike($column, $value)
 **/
class Author extends Model
{
	protected $fillable = ['name', 'surname', 'biography'];

	protected $table = 'author';

	public function reviews() {
		return $this->hasMany(Book::class, 'author_id', 'id');
	}

	public function scopeAuthorLike(Builder $query, $column, $value)
	{
		return $query->where( $column, 'like', "%$value%" )->get();
	}

}