<?php


namespace App\Entity;


use Illuminate\Database\Eloquent\Model;

class BookCategory extends Model
{
	protected $fillable = ['category_id', 'book_id'];

	protected $table = 'book_category';
	public $timestamps = false;

}