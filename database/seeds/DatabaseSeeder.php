<?php

use App\Entity\Category;
use App\Entity\Author;
use App\Entity\Book;
use App\Entity\BookCategory;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(UsersTableSeeder::class);
	    factory(User::class, 10)->create();
	    factory(Category::class, 2)->create();
	    factory(Author::class, 20)->create();
	    factory(Book::class, 50)->create();
	    factory(BookCategory::class, 50)->create();
    }
}
