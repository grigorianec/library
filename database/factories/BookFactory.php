<?php


/** @var Factory $factory */

use App\Entity\Book;
use App\Entity\Author;
use Illuminate\Database\Eloquent\Factory;
use Faker\Generator as Faker;

$factory->define( Book::class, function (Faker $faker) {
	return [
		'name' => $faker->name,
		'description' => $faker->text(min([50])),
		'link' => $faker->url,
		'author_id' => Author::inRandomOrder()->value('id'),
	];
});