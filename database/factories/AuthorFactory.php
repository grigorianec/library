<?php


/** @var Factory $factory */

use App\Entity\Author as AuthorAlias;
use Illuminate\Database\Eloquent\Factory;
use Faker\Generator as Faker;

$factory->define( AuthorAlias::class, function (Faker $faker) {
	return [
		'name' => $faker->firstName,
		'surname' => $faker->lastName,
		'biography' => $faker->text(min([50])),
	];
});