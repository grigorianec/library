<?php

/** @var Factory $factory */

use App\Entity\Category;
use Illuminate\Database\Eloquent\Factory;



$factory->define( Category::class, function () {
	$genre = ['documentary' => 1,'drama' => 2];
	return [
		'name' => array_rand($genre)
	];
});