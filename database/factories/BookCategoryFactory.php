<?php

/** @var Factory $factory */

use App\Entity\BookCategory;
use Illuminate\Database\Eloquent\Factory;

$factory->define( BookCategory::class, function () {
	return [
		'category_id' => App\Entity\Category::inRandomOrder()->value('id'),
		'book_id' => App\Entity\Book::inRandomOrder()->value('id'),
	];
});