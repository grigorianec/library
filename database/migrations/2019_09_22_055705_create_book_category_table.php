<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_category', function (Blueprint $table) {
            $table->bigIncrements('id');
	        $table->bigInteger('category_id')->unsigned();
	        $table->bigInteger('book_id')->unsigned();
        });
	    Schema::table('book_category', function($table) {
		    $table->foreign('category_id')->references('id')->on('category')->onDelete('CASCADE');
		    $table->foreign('book_id')->references('id')->on('book')->onDelete('CASCADE');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_book');
    }
}
