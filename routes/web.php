<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('home');


Auth::routes();

Route::group([
	'prefix' => 'admin',
	'as' => 'admin.',
	'namespace' => 'Admin',
	'middleware' => ['auth']
], function () {
	Route::get('/home', 'HomeController@index')->name('home');
	Route::group( [
		'prefix'    => 'author',
		'as'        => 'authors.',
	], function () {
		Route::get( '/', 'AuthorController@index' )->name( 'home' );
		Route::get( '/show/{author}', 'AuthorController@show' )->name( 'show' );
		Route::get( '/create', 'AuthorController@create' )->name( 'create' );
		Route::post( '/store', 'AuthorController@store' )->name( 'store' );
		Route::get( '/{author}/edit', 'AuthorController@edit' )->name( 'edit' );
		Route::put( '/{author}/update', 'AuthorController@update' )->name( 'update' );
		Route::delete( '/{author}/delete', 'AuthorController@destroy' )->name( 'delete' );
	});

	Route::group( [
		'prefix'    => 'book',
		'as'        => 'books.',
	], function () {
		Route::get( '/', 'BookController@index' )->name( 'index' );
		Route::get( '/ajax', 'BookController@index' )->name( 'searchAjax' );
		Route::get( '/show/{book}', 'BookController@show' )->name( 'show' );
		Route::get( '/create', 'BookController@create' )->name( 'create' );
		Route::post( '/store', 'BookController@store' )->name( 'store' );
		Route::get( '/{book}/edit', 'BookController@edit' )->name( 'edit' );
		Route::put( '/{book}/update', 'BookController@update' )->name( 'update' );
		Route::delete( '/{book}/delete', 'BookController@destroy' )->name( 'delete' );
	});

	Route::group( [
		'prefix'    => 'category',
		'as'        => 'categories.',
	], function () {
		Route::get( '/', 'CategoryController@index' )->name( 'index' );
		Route::get( '/show/{category}', 'CategoryController@show' )->name( 'show' );
		Route::get( '/create', 'CategoryController@create' )->name( 'create' );
		Route::post( '/store', 'CategoryController@store' )->name( 'store' );
		Route::get( '/{category}/edit', 'CategoryController@edit' )->name( 'edit' );
		Route::put( '/{category}/update', 'CategoryController@update' )->name( 'update' );
		Route::delete( '/{category}/delete', 'CategoryController@destroy' )->name( 'delete' );
	});
  });


