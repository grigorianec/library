<?php

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator as Crumbs;

Breadcrumbs::register('home', function (Crumbs $crumbs) {
	$crumbs->push('Home', route('home'));
});

Breadcrumbs::register('login', function (Crumbs $crumbs) {
	$crumbs->parent('home');
	$crumbs->push('Login', route('login'));
});

Breadcrumbs::register('register', function (Crumbs $crumbs) {
	$crumbs->parent('home');
	$crumbs->push('Register', route('register'));
});

Breadcrumbs::register('reset', function (Crumbs $crumbs) {
	$crumbs->parent('home');
	$crumbs->parent('login');
	$crumbs->push('Register', route('reset'));
});

Breadcrumbs::register('admin.authors', function (Crumbs $crumbs) {
	$crumbs->push('Home', route('home'));
});

Breadcrumbs::register('admin.home', function (Crumbs $crumbs) {
	$crumbs->push('Home', route('admin.home'));
});

Breadcrumbs::register('admin.authors.home', function (Crumbs $crumbs) {
	$crumbs->push('Home', route('home'));
	$crumbs->push('Authors', route('admin.authors.home'));
});

Breadcrumbs::register( 'admin.authors.show', function (Crumbs $crumbs, Author $author) {
	$crumbs->parent('admin.authors.home');
	$crumbs->push($author->name, route('admin.authors.show', $author));
});
//
Breadcrumbs::register('admin.authors.create', function (Crumbs $crumbs) {
	$crumbs->parent('admin.authors.home');
	$crumbs->push('create', route('admin.authors.create'));
});

Breadcrumbs::register('admin.authors.edit', function (Crumbs $crumbs, Author $author) {
	$crumbs->parent('admin.authors');
	$crumbs->push($author->name, route('admin.authors.show', $author));
	$crumbs->push('edit', route('admin.authors.edit', $author));
});


Breadcrumbs::register('admin.books.index', function (Crumbs $crumbs) {
	$crumbs->push('Home', route('home'));
	$crumbs->push('Books', route('admin.books.index'));
});

Breadcrumbs::register( 'admin.books.show', function (Crumbs $crumbs, Book $book) {
	$crumbs->parent('admin.books.index');
	$crumbs->push($book->name, route('admin.books.show', $book));
});

Breadcrumbs::register('admin.books.create', function (Crumbs $crumbs) {
	$crumbs->parent('admin.books.index');
	$crumbs->push('create', route('admin.books.create'));
});

Breadcrumbs::register('admin.books.edit', function (Crumbs $crumbs, Book $book) {
	$crumbs->parent('admin.books.index');
	$crumbs->push($book->name, route('admin.books.show', $book));
	$crumbs->push('edit', route('admin.books.edit', $book));
});

Breadcrumbs::register('admin.categories.index', function (Crumbs $crumbs) {
	$crumbs->push('Home', route('home'));
	$crumbs->push('Categories', route('admin.categories.index'));
});

Breadcrumbs::register( 'admin.categories.show', function (Crumbs $crumbs, Category $category) {
	$crumbs->parent('admin.categories.index');
	$crumbs->push($category->name, route('admin.categories.show', $category));
});

Breadcrumbs::register('admin.categories.create', function (Crumbs $crumbs) {
	$crumbs->parent('admin.categories.index');
	$crumbs->push('create', route('admin.categories.create'));
});

Breadcrumbs::register('admin.categories.edit', function (Crumbs $crumbs, Category $category) {
	$crumbs->parent('admin.categories.index');
	$crumbs->push($category->name, route('admin.categories.show', $category));
	$crumbs->push('edit', route('admin.categories.edit', $category));
});